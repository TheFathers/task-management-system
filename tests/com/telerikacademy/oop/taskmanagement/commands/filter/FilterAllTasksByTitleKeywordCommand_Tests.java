package com.telerikacademy.oop.taskmanagement.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByTitleKeywordCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByTitleKeywordCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_TITLE;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class FilterAllTasksByTitleKeywordCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new FilterAllTasksByTitleKeywordCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act

        List<String> arguments = List.of(BUG_VALID_TITLE);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }


    @Test
    public void execute_should_executeWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TITLE);

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeDoneStatus_when_inputIsValid() {
        // Arrange, Act

        Story story = initializeStory(managementSystemRepository);
        story.changeStoryStatus(StoryStatus.DONE);
        Feedback feedback = initializeFeedback(managementSystemRepository);
        feedback.changeFeedbackStatus(FeedbackStatus.DONE);

        List<String> arguments = List.of("Done");

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}

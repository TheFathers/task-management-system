package com.telerikacademy.oop.taskmanagement.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByAssigneeCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByAssigneeCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.MEMBER_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class FilterAllTasksByAssigneeCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new FilterAllTasksByAssigneeCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act
        initializeMember(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME);

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}

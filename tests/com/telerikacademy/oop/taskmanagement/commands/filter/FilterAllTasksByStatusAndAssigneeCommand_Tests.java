package com.telerikacademy.oop.taskmanagement.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByStatusAndAssigneeCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByStatusAndAssigneeCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.MEMBER_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_BUGSTATUS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.STORY_VALID_STORYSTATUS;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class FilterAllTasksByStatusAndAssigneeCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new FilterAllTasksByStatusAndAssigneeCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act
        initializeMember(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME, STORY_VALID_STORYSTATUS.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_memberIsNotAssigned() {
        // Arrange, Act
        initializeMember(managementSystemRepository);
        initializeStory(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME, STORY_VALID_STORYSTATUS.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_statusIsInvalid() {
        // Arrange, Act

        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME, "Invalid_Status_input");

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeBugStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeAssignedBug(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME, BUG_VALID_BUGSTATUS.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeStoryStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of(MEMBER_VALID_NAME, STORY_VALID_STORYSTATUS.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}

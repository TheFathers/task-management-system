package com.telerikacademy.oop.taskmanagement.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.filter.FilterTaskTypeByAssigneeCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;

import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.filter.FilterTaskTypeByAssigneeCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.MEMBER_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.STORY_VALID_TASKTYPE;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class FilterTaskTypeByAssigneeCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new FilterTaskTypeByAssigneeCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act
        initializeMember(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString(), MEMBER_VALID_NAME);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_tasktypeIsInvalid() {
        // Arrange, Act
        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of(TaskType.FEEDBACK.toString(), MEMBER_VALID_NAME);

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_member_IsNotAssigned() {
        // Arrange, Act
        initializeMember(managementSystemRepository);
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString(), MEMBER_VALID_NAME);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeBugStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeAssignedBug(managementSystemRepository);
        List<String> arguments = List.of( BUG_VALID_TASKTYPE.toString(), MEMBER_VALID_NAME);

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeStoryStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of( STORY_VALID_TASKTYPE.toString(), MEMBER_VALID_NAME);

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}
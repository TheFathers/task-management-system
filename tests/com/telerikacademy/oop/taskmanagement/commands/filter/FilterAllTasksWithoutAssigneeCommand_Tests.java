package com.telerikacademy.oop.taskmanagement.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksWithoutAssigneeCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class FilterAllTasksWithoutAssigneeCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new FilterAllTasksWithoutAssigneeCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act

        List<String> arguments = List.of();

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeStory(managementSystemRepository);
        List<String> arguments = List.of();

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}

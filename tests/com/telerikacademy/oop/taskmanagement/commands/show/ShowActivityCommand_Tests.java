package com.telerikacademy.oop.taskmanagement.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.show.ShowActivityCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Nameable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;


public class ShowActivityCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ShowActivityCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_executeWithoutException_when_input_Member_IsValid() {
        // Arrange, Act
        Nameable member = initializeMember(managementSystemRepository);

        command.execute(List.of(member.getName()));

        // Assert
        Assertions.assertEquals(1, managementSystemRepository.getFreePeople().size());
    }

    @Test
    public void execute_should_executeWithoutException_when_input_Team_IsValid() {
        // Arrange, Act
        Nameable team = initializeTeam(managementSystemRepository);

        command.execute(List.of(team.getName()));

        // Assert
        Assertions.assertEquals(1, managementSystemRepository.getTeams().size());
    }

    @Test
    public void execute_should_executeWithoutException_when_input_Board_IsValid() {
        // Arrange, Act
        Nameable board = initializeBoard(managementSystemRepository);

        command.execute(List.of(board.getName()));
        List<Board> boards = managementSystemRepository.getTeams().stream()
                .flatMap(team -> team.getBoards().stream())
                .collect(Collectors.toList());

        // Assert
        Assertions.assertEquals(1,boards.size());
    }

    @Test
    public void execute_should_throwException_when_Name_doesNot_Exist() {
        // Arrange, Act
        List<String> arguments = List.of("Not,a,valid,name");

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

}

package com.telerikacademy.oop.taskmanagement.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.sorting.SortTaskTypeByRatingCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.initializeFeedback;

public class SortTaskTypeByRating_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new SortTaskTypeByRatingCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_thereIsNoFeedbacks() {
        // Arrange, Act

        List<String> arguments = List.of();

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeFeedback(managementSystemRepository);
        List<String> arguments = List.of();

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
    /*


    SortTaskTypeBySize
            SortTaskTypeByTitle*/
}

package com.telerikacademy.oop.taskmanagement.commands.change;

import com.telerikacademy.oop.managementsystem.commands.change.ChangeBugCommand;
import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.enums.BugFeature;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.change.ChangeBugCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class ChangeBugCommand_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ChangeBugCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_idIsInvalid() {
        // Arrange, Act

        List<String> arguments = List.of(
                "Invalid-ID-Input",
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_BugDoesNotExist() {
        // Arrange, Act

        List<String> arguments = List.of(
                String.valueOf(BUG_VALID_ID),
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_BugFeatureIsInvalid() {
        // Arrange, Act

        List<String> arguments = List.of(
                String.valueOf(BUG_VALID_ID),
                "Invalid-Bug-Feature",
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_PrioritIsinvalid() {
        // Arrange, Act
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(
                String.valueOf(BUG_VALID_ID),
                BUG_VALID_FEATURE.toString(),
                "Invalid-Priority-Input");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_changeHighToLowPriority_when_inputIsValid() {
        // Arrange, Act
        Bug bug = initializeBug(managementSystemRepository);
        bug.changePriority(Priority.HIGH);
        command.execute(List.of(
                String.valueOf(BUG_VALID_ID),
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString()));

        // Assert
        Assertions.assertEquals(BUG_VALID_PRIORITY, bug.getPriority());
    }

    @Test
    public void execute_should_changeMinorToCriticalSeverity_when_inputIsValid() {
        // Arrange, Act
        Bug bug = initializeBug(managementSystemRepository);

        command.execute(List.of(
                String.valueOf(BUG_VALID_ID),
                BugFeature.SEVERITY.toString(),
                Severity.CRITICAL.toString()));

        // Assert
        Assertions.assertEquals(Severity.CRITICAL, bug.getSeverity());
    }

    @Test
    public void execute_should_changeActiveToFixedStatus_when_inputIsValid() {
        // Arrange, Act
        Bug bug = initializeBug(managementSystemRepository);
        command.execute(List.of(
                String.valueOf(BUG_VALID_ID),
                BugFeature.STATUS.toString(),
                BugStatus.FIXED.toString()));

        // Assert
        Assertions.assertEquals(BugStatus.FIXED.toString(), bug.getStatus());
    }

}

package com.telerikacademy.oop.taskmanagement.commands.change;

import com.telerikacademy.oop.managementsystem.commands.change.ChangeStoryCommand;
import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.change.ChangeStoryCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_ID;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class ChangeStoryCommand_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ChangeStoryCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_idIsInvalid() {
        // Arrange, Act

        List<String> arguments = List.of(
                "Invalid-ID-Input",
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_storyDoesNotExist() {
        // Arrange, Act

        List<String> arguments = List.of(
                String.valueOf(STORY_VALID_ID),
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_StoryFeatureIsInvalid() {
        // Arrange, Act

        List<String> arguments = List.of(
                String.valueOf(STORY_VALID_ID),
                "Invalid-Bug-Feature",
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_PrioritIsinvalid() {
        // Arrange, Act
        initializeStory(managementSystemRepository);
        List<String> arguments = List.of(
                String.valueOf(STORY_VALID_ID),
                STORY_VALID_FEATURE.toString(),
                "Invalid-Priority-Input");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_changeHighToLowPriority_when_inputIsValid() {
        // Arrange, Act
        Story story = initializeStory(managementSystemRepository);
        story.changePriority(Priority.HIGH);
        command.execute(List.of(
                String.valueOf(STORY_VALID_ID),
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString()));

        // Assert
        Assertions.assertEquals(STORY_VALID_PRIORITY, story.getPriority());
    }

    @Test
    public void execute_should_changeMediumToSmall_when_inputIsValid() {
        // Arrange, Act
        Story story = initializeStory(managementSystemRepository);
        story.changeStorySize(StorySize.MEDIUM);
        command.execute(List.of(
                String.valueOf(BUG_VALID_ID),
                StoryFeature.SIZE.toString(),
                StorySize.LARGE.toString()));

        // Assert
        Assertions.assertEquals(StorySize.LARGE, story.getStorySize());
    }

    @Test
    public void execute_should_changeActiveToFixedStatus_when_inputIsValid() {
        // Arrange, Act
        Story story = initializeStory(managementSystemRepository);
        story.changeStoryStatus(StoryStatus.INPROGRESS);
        command.execute(List.of(
                String.valueOf(BUG_VALID_ID),
                StoryFeature.STATUS.toString(),
                StoryStatus.DONE.toString()));

        // Assert
        Assertions.assertEquals(StoryStatus.DONE.toString(), story.getStatus());
    }

}

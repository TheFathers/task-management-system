package com.telerikacademy.oop.taskmanagement.commands.change;

import com.telerikacademy.oop.managementsystem.commands.change.ChangeFeedbackCommand;
import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.enums.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.change.ChangeFeedbackCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Feedback.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.initializeListWithSize;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.initializeFeedback;

public class ChangeFeedbackCommand_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ChangeFeedbackCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_idIsInvalid() {
        // Arrange, Act

        List<String> arguments = List.of(
                "Invalid-ID-Input",
                FEEDBACK_VALID_FEATURE.toString(),
                String.valueOf(FEEDBACK_VALID_RATING));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_storyDoesNotExist() {
        // Arrange, Act

        List<String> arguments = List.of(
                String.valueOf(FEEDBACK_VALID_ID),
                FEEDBACK_VALID_FEATURE.toString(),
                String.valueOf(FEEDBACK_VALID_RATING));

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_StoryFeatureIsInvalid() {
        // Arrange, Act

        List<String> arguments = List.of(
                String.valueOf(FEEDBACK_VALID_ID),
                "Invalid-Feedback-Feature",
                String.valueOf(FEEDBACK_VALID_RATING));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_RatingIsinvalid() {
        // Arrange, Act
        initializeFeedback(managementSystemRepository);
        List<String> arguments = List.of(
                String.valueOf(FEEDBACK_VALID_ID),
                FEEDBACK_VALID_FEATURE.toString(),
                "Invalid-Rating-Input");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_changeOneToThreeRating_when_inputIsValid() {
        // Arrange, Act
        Feedback feedback = initializeFeedback(managementSystemRepository);
        feedback.changeRating(2);
        command.execute(List.of(
                String.valueOf(FEEDBACK_VALID_ID),
                FEEDBACK_VALID_FEATURE.toString(),
                String.valueOf(3)));

        // Assert
        Assertions.assertEquals(3, feedback.getRating());
    }

    @Test
    public void execute_should_changeNewToScheduled_when_inputIsValid() {
        // Arrange, Act
        Feedback feedback = initializeFeedback(managementSystemRepository);
        feedback.changeFeedbackStatus(FeedbackStatus.UNSCHEDULED);
        command.execute(List.of(
                String.valueOf(FEEDBACK_VALID_ID),
                FeedbackFeature.FEEDBACKSTATUS.toString(),
                FeedbackStatus.SCHEDULED.toString()));

        // Assert
        Assertions.assertEquals(FeedbackStatus.SCHEDULED.toString(), feedback.getStatus());
    }

}

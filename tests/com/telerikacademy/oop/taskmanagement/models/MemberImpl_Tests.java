package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.models.MemberImpl;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.managementsystem.models.MemberImpl.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class MemberImpl_Tests {

    @Test
    public void constructor_should_throwException_when_nameLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MemberImpl(initializeStringWithSize(NAME_MIN_LENGTH-1)));
    }

    @Test
    public void addTask_should_addTask_when_inputIsValid() {
        //Arrange
        Task task = createBug();
        Member member = createMember();
        member.addTask(task);

        //Act, Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }

    @Test
    public void removeTask_should_removeTask_when_inputIsValid() {
        //Arrange
        Task task = createBug();
        Member member = createMember();
        member.addTask(task);
        member.removeTask(task);

        //Act, Assert
        Assertions.assertEquals(0, member.getTasks().size());
    }
}

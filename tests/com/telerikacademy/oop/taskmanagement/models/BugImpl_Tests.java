package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.managementsystem.models.BugImpl.REPRODUCING_STEPS_MIN_LENGTH;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_REPRODUCING_STEPS;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class BugImpl_Tests {
    @Test
    public void changePriority_throwsException_whenThePriorityIsTheSame() {
        // Arrange
        Bug bug = createBug();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->  bug.changePriority(Priority.LOW));
    }

    @Test
    public void changeSeverity_throwsException_whenTheSeverityIsTheSame() {
        // Arrange
        Bug bug = createBug();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->  bug.changeSeverity(Severity.MINOR));
    }

    @Test
    public void changeBugStatus_throwsException_whenTheBugStatusIsTheSame() {
        // Arrange
        Bug bug = createBug();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->  bug.changeStatus(BugStatus.ACTIVE));
    }

    @Test
    public void addReproducingSteps_throwsException_whenReproducingStepsLengthIsInvalid() {
        // Arrange
        Bug bug = createBug();
        String invalidReproducingSteps = initializeStringWithSize(REPRODUCING_STEPS_MIN_LENGTH-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,() ->
                bug.addReproducingSteps(invalidReproducingSteps));
    }

    @Test
    public void addReproducingSteps_throwsException_whenReproducingStepsLengthIsValid() {
        // Arrange
        Bug bug = createBug();

        //Act
        bug.addReproducingSteps(BUG_VALID_REPRODUCING_STEPS);

        // Assert
        Assertions.assertEquals(1, bug.getReproducingSteps().size());
    }
}

package com.telerikacademy.oop.managementsystem.models;

public record CommandResult(
        String message
) {
}

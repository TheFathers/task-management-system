package com.telerikacademy.oop.managementsystem.models.dtos;

public record PersonDto(String personName) {
}

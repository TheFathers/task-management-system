package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.enums.*;

public interface Story extends Task, Assignable {

    Priority getPriority();

    StorySize getStorySize();

    void changePriority(Priority priority);

    void changeStorySize(StorySize storySize);

    void changeStoryStatus(StoryStatus storyStatus);
}

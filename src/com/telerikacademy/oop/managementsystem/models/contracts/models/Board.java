package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.Manageable;

import java.util.List;

public interface Board extends Manageable {

    List<Task> getTasks();

    void addTask(Task task);

    void removeTask(Task task);
}

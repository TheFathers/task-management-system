package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.*;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;

public interface Task extends Identifiable, LogActivityAble, Printable, Commentable {

    String getTitle();

    String getDescription();

    String getStatus();

    String getAdditionalInfo();

    String getImportantInfo();

    TaskType getTaskType();
}

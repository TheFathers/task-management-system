package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.Severity;

import java.util.List;

public interface Bug extends Task, Assignable {

    Priority getPriority();

    Severity getSeverity();

    List<String> getReproducingSteps();

    void changePriority(Priority priority);

    void changeSeverity(Severity severity);

    void changeStatus(BugStatus status);

    void addReproducingSteps(String steps);
}

package com.telerikacademy.oop.managementsystem.models.contracts.ables;

public interface Manageable extends LogActivityAble, Nameable{
}

package com.telerikacademy.oop.managementsystem.models;


import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Commentable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.Severity;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;


import java.util.ArrayList;
import java.util.List;

public class BugImpl extends TaskBaseImpl implements Bug, Commentable, Assignable {


    public static final String SAME_BUG_PRIORITY = "Bug priority is already %s";
    public static final String SAME_BUG_SEVERITY = "Bug severity is already %s";
    public static final String SAME_BUG_STATUS = "Bug status is already %s";
    public static final String BUG_PRIORITY_CHANGE = "Bug priority changed from %s to %s.";
    public static final String BUG_SEVERITY_CHANGE = "Bug severity changed from %s to %s.";
    public static final String BUG_CREATED = "Bug with id %d created.";
    public static final String BUG_STATUS_CHANGED = "Bug status changed from %s to %s.";
    public static final String BUG_ASSIGNED_TO = "Bug assigned to %s.";
    public static final String BUG_UNASSIGNED_FROM = "Bug unassigned from %s";
    public static final int REPRODUCING_STEPS_MIN_LENGTH = 10;
    public static final int REPRODUCING_STEPS_MAX_LENGTH = 150;
    public static final String INVALID_REPRODUCING_STEPS_LENGTH =
            String.format("Please enter reproducing steps between %d and %d symbols long",
                    REPRODUCING_STEPS_MIN_LENGTH,
                    REPRODUCING_STEPS_MAX_LENGTH);
    private Priority priority;
    private Severity severity;
    private BugStatus bugStatus;
    private final List<Member> assignees; //null
    private final List<String> reproducingSteps;


    public BugImpl(int id, String title, String description) {
        super(id, title, description);
        setPriority(Priority.LOW);
        setSeverity(Severity.MINOR);
        setBugStatus(BugStatus.ACTIVE);
        assignees = new ArrayList<>();
        reproducingSteps = new ArrayList<>();
        logActivity(new ActivityImpl(String.format(BUG_CREATED, super.getId())));
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    private void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    private void setBugStatus(BugStatus bugStatus) {
        this.bugStatus = bugStatus;
    }

    @Override
    public String getStatus() {
        return bugStatus.toString();
    }

    @Override
    public TaskType getTaskType() {
        return TaskType.BUG;
    }

    @Override
    public void changePriority(Priority priority) {
        if (priority.equals(this.priority)){
            throw new InvalidUserInputException(String.format(SAME_BUG_PRIORITY, priority));
        }
        logActivity(new ActivityImpl(String.format(BUG_PRIORITY_CHANGE, this.priority, priority)));
        setPriority(priority);
    }

    @Override
    public void changeSeverity(Severity severity) {
        if (severity.equals(this.severity)){
            throw new InvalidUserInputException(String.format(SAME_BUG_SEVERITY, severity));
        }
        logActivity(new ActivityImpl(String.format(BUG_SEVERITY_CHANGE, this.severity, severity)));
        setSeverity(severity);
    }

    @Override
    public void changeStatus(BugStatus bugStatus) {
        if (bugStatus.equals(this.bugStatus)){
            throw new InvalidUserInputException(String.format(SAME_BUG_STATUS, bugStatus));
        }
        logActivity(new ActivityImpl(String.format(BUG_STATUS_CHANGED, this.bugStatus, bugStatus)));
        setBugStatus(bugStatus);
    }

    @Override
    public void addReproducingSteps(String steps) {
        ValidationHelpers.validateStringLength(
                steps,
                REPRODUCING_STEPS_MIN_LENGTH,
                REPRODUCING_STEPS_MAX_LENGTH,
                INVALID_REPRODUCING_STEPS_LENGTH
        );
        reproducingSteps.add(steps);
    }

    @Override
    public void setAssignees(List<Member> members) {
        logActivity(new ActivityImpl(String.format(BUG_ASSIGNED_TO, members.toString())));
        this.assignees.addAll(members);
    }

    @Override
    public void removeAssignees(List<Member> members) {
        logActivity(new ActivityImpl(String.format(BUG_UNASSIGNED_FROM, members.toString())));
        this.assignees.removeAll(members);
    }

    @Override
    public List<Member> getAssignees() {
        return new ArrayList<>(assignees);
    }

    @Override
    public List<String> getReproducingSteps() {
        return new ArrayList<>(reproducingSteps);
    }

    @Override
    public String getAdditionalInfo() {
        return String.format("Priority: %s%n" +
                        "Severity: %s%n" +
                        "Status: %s%n" +
                        "Assignee: %s%n" +
                        "Reproducing step: %s",
                priority,
                severity,
                bugStatus,
                assignees,
                reproducingSteps);
    }
}

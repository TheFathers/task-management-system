package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {
    public static final String ASSIGNED_MSG = "%s with ID %d assigned to %s";
    public static final String UNASSIGNED_MSG = "%s with ID %d unassigned to %s";
    public static final String PERSON_CREATED = "Person %s was created.";
    public static int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String INVALID_NAME_LENGTH = String.format(
                    "Invalid input. Name should be between %d and %d symbols long.",
                    NAME_MIN_LENGTH,
                    NAME_MAX_LENGTH);

    private String name;
    private final List<Task> tasks;
    private final List<Activity> history;


    public MemberImpl(String name) {
        setName(name);
        this.tasks = new ArrayList<>();
        this.history = new ArrayList<>();
        history.add(new ActivityImpl(String.format(PERSON_CREATED, name)));
    }

    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(
                name,
                NAME_MIN_LENGTH,
                NAME_MAX_LENGTH,
                INVALID_NAME_LENGTH);
        this.name = name;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }


    @Override
    public void addTask(Task task) {
        tasks.add(task);
        logActivity(new ActivityImpl(String.format(
                ASSIGNED_MSG, task.getTaskType(), task.getId(), name)));
    }

    @Override
    public void removeTask(Task task) {
        logActivity(new ActivityImpl(String.format(
                UNASSIGNED_MSG, task.getTaskType(), task.getId(), name)));
        tasks.remove(task);
    }

    @Override
    public List<Activity> getActivity() {
        return new ArrayList<>(history);
    }

    @Override
    public void logActivity(Activity event) {
        history.add(event);
    }

    @Override
    public String toString() {
        return getName();
    }
}

package com.telerikacademy.oop.managementsystem.models.enums;

import com.telerikacademy.oop.managementsystem.models.BugImpl;
import com.telerikacademy.oop.managementsystem.models.FeedbackImpl;
import com.telerikacademy.oop.managementsystem.models.StoryImpl;
import com.telerikacademy.oop.managementsystem.models.TaskBaseImpl;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;

public enum TaskType {
    BUG {
        @Override
        public Task createInstance(int id, String title, String description) {
            return new BugImpl(id, title, description);
        }
    },
    STORY {
        @Override
        public Task createInstance(int id, String title, String description) {
            return new StoryImpl(id, title, description);
        }
    },
    FEEDBACK {
        @Override
        public Task createInstance(int id, String title, String description) {
            return new FeedbackImpl(id, title, description);
        }
    };

    @Override
    public String toString() {
        return switch (this) {
            case BUG -> "Bug";
            case STORY -> "Story";
            case FEEDBACK -> "Feedback";
        };
    }

    public abstract Task createInstance(int id, String title, String description);
}
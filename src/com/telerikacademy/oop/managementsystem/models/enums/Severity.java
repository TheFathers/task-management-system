package com.telerikacademy.oop.managementsystem.models.enums;

public enum Severity {
    CRITICAL, MAJOR, MINOR
}

package com.telerikacademy.oop.managementsystem.models.enums;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;

import static com.telerikacademy.oop.managementsystem.utils.ParsingHelpers.tryParseEnum;

public enum BugFeature implements Features<Bug> {
    PRIORITY {
        @Override
        public void change(Bug bug, String newValue) {
            bug.changePriority(tryParseEnum(newValue, Priority.class));
        }
    },
    SEVERITY {
        @Override
        public void change(Bug bug, String newValue) {
            bug.changeSeverity(tryParseEnum(newValue, Severity.class));
        }
    },
    STATUS {
        @Override
        public void change(Bug bug, String newValue) {
            bug.changeStatus(tryParseEnum(newValue, BugStatus.class));
        }
    };

    @Override
    public String toString() {
        return switch (this) {
            case PRIORITY -> "Priority";
            case SEVERITY -> "Severity";
            case STATUS -> "Status";
        };
    }
}

package com.telerikacademy.oop.managementsystem.events;

import an.awesome.pipelinr.Notification;
import com.telerikacademy.oop.managementsystem.models.CommandResult;

public interface CreatePersonNotification extends Notification {
}

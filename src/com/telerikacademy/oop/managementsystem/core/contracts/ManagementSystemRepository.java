package com.telerikacademy.oop.managementsystem.core.contracts;

import an.awesome.pipelinr.Notification;
import an.awesome.pipelinr.Pipeline;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Identifiable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.*;

import java.util.List;

public interface ManagementSystemRepository {
    List<Member> getFreePeople();

    List<Member> getAllMembers();

    List<Team> getTeams();

    List<Task> getAllTasks();

    List<Bug> getAllBugs();

    List<Story> getAllStories();

    List<Feedback> getAllFeedbacks();

    List<Notification> getNotifications();

    boolean personExists(String personName);

    boolean memberExists(String memberName);

    boolean teamExists(String teamName);

    boolean boardExists(String boardName);

    boolean bugExists(int id);

    boolean storyExists(int id);

    boolean feedbackExists(int id);

    boolean boardInTeamExists(String boardName, String teamName);

    void addPerson(String name);

    void addTeam(String name);

    void addNotification(Notification notification);

    void dispatchNotifications(Pipeline pipeline);

    Board createBoard(String title);

    Story createStory(String title, String description);

    Feedback createFeedback(String title, String description);

    void addPersonToTeam(Member freePerson, Team team);

    Member findPersonByName(String personName);

    Member findMemberByName(String personName);

    Team findTeamByName(String teamName);

    Board findBoardByName(String boardName);

    Board findBoardByTeamName(String boardName, String teamName);

    Task findTaskById(int id);

    Bug findBugById(int id);

    Story findStoryById(int id);

    Feedback findFeedbackById(int id);

    <T extends Identifiable> T findItemById(int id, Class<T> clazz);

}



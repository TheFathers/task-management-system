package com.telerikacademy.oop.managementsystem.core;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Notification;
import an.awesome.pipelinr.Pipeline;
import an.awesome.pipelinr.Pipelinr;
import com.telerikacademy.oop.managementsystem.commands.creation.*;
import com.telerikacademy.oop.managementsystem.core.contracts.CommandFactory;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemEngine;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.events.CreatePersonCompletedNotificationHandler;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.validation.ValidationMiddleware;
import com.telerikacademy.oop.managementsystem.validation.validators.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class ManagementSystemEngineImpl implements ManagementSystemEngine {

    private static final String TERMINATION_COMMAND = "Exit";
    private static final String EMPTY_COMMAND_ERROR = "Command cannot be empty.";
    private static final String MAIN_SPLIT_SYMBOL = " ";
    private static final String COMMENT_OPEN_SYMBOL = "{{";
    private static final String COMMENT_CLOSE_SYMBOL = "}}";
    private static final String REPORT_SEPARATOR = "--------------------";

    private final CommandFactory commandFactory;
    private final ManagementSystemRepository managementSystemRepository;

    private final Pipeline pipeline;

    public ManagementSystemEngineImpl() {
        this.commandFactory = new CommandFactoryImpl();
        this.managementSystemRepository = new ManagementSystemRepositoryImpl();
        this.pipeline = new Pipelinr()
                .with(() -> Stream.of(
                        new CreatePerson.CommandHandler(),
                        new CreateBoard.CommandHandler(),
                        new AddComment.Handler(),
                        new AddMember.CommandHandler(),
                        new AddBugReproducingSteps.CommandHanler()
                ))
                .with(() -> Stream.of(new ValidationMiddleware(List.of(
                        AddCommentValidator.INSTANCE,
                        AddMemberValidator.INSTANCE,
                        AddBugReproducingStepsValidator.INSTANCE,
                        CreatePersonValidator.INSTANCE,
                        CreateBoardValidator.INSTANCE
                ))))
                .with(() -> Stream.of(new CreatePersonCompletedNotificationHandler()));
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                String inputLine = scanner.nextLine();
                if (inputLine.isBlank()) {
                    print(EMPTY_COMMAND_ERROR);
                    continue;
                }
                if (inputLine.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(inputLine);
            } catch (Exception ex) {
                if (ex.getMessage() != null && !ex.getMessage().isEmpty()) {
                    print(ex.getMessage());
                } else {
                    print(ex.toString());
                }
            }
        }
    }

    private void processCommand(String inputLine) {
        String commandName = extractCommandName(inputLine);
        List<String> parameters = extractCommandParameters(inputLine);
        Command<CommandResult> command = commandFactory.createCommandFromCommandName(commandName, parameters, managementSystemRepository);
        String executionResult = command.execute(pipeline).message();
        print(executionResult);
        managementSystemRepository.dispatchNotifications(pipeline);
    }

    /**
     * Receives a full line and extracts the command to be executed from it.
     * For example, if the input line is "FilterBy Assignee John", the method will return "FilterBy".
     *
     * @param inputLine A complete input line
     * @return The name of the command to be executed
     */
    private String extractCommandName(String inputLine) {
        return inputLine.split(" ")[0];
    }

    /**
     * Receives a full line and extracts the parameters that are needed for the command to execute.
     * For example, if the input line is "FilterBy Assignee John",
     * the method will return a list of ["Assignee", "John"].
     *
     * @param inputLine A complete input line
     * @return A list of the parameters needed to execute the command
     */
    private List<String> extractCommandParameters(String inputLine) {
        if (inputLine.contains(COMMENT_OPEN_SYMBOL)) {
            return extractCommentParameters(inputLine);
        }
        String[] commandParts = inputLine.split(" ");
        return new ArrayList<>(Arrays.asList(commandParts).subList(1, commandParts.length));
    }

    /**
     * Receives a line that contains comments and extracts the parameters that are needed for the command to execute.
     * Sample input: CreateTask {{Christmas is coming}} {{Holidays are on their way}} Trainers T-board Story
     * The method will return a list of ["Christmas is coming", "Holidays are on their way", "Trainers", "T-board", "Story"].
     *
     * @param fullCommand A complete input line
     * @return A list of the parameters needed to execute the command
     */
    public static List<String> extractCommentParameters(String fullCommand) {
        String commandParameters = fullCommand.substring(fullCommand.indexOf(MAIN_SPLIT_SYMBOL) + 1);
        int indexOfOpenComment = commandParameters.indexOf(COMMENT_OPEN_SYMBOL);
        List<String> parameters = new ArrayList<>();
        int currentParameterStartIndex = 0;
        int currentParameterStopIndex, nextParameterStartIndex, indexOfFirstSeparator, indexOfCloseComment;

        while (indexOfOpenComment >= 0) {
            indexOfFirstSeparator = commandParameters.indexOf(MAIN_SPLIT_SYMBOL);
            indexOfOpenComment = commandParameters.indexOf(COMMENT_OPEN_SYMBOL);
            indexOfCloseComment = commandParameters.indexOf(COMMENT_CLOSE_SYMBOL);
            if (indexOfOpenComment == 0) {
                currentParameterStartIndex = indexOfOpenComment + COMMENT_CLOSE_SYMBOL.length();
                currentParameterStopIndex = indexOfCloseComment;
                indexOfCloseComment += COMMENT_CLOSE_SYMBOL.length();
                nextParameterStartIndex = (indexOfCloseComment == commandParameters.length()) ?
                        indexOfCloseComment : indexOfCloseComment + 1;
            } else if (indexOfOpenComment > 0) {
                currentParameterStopIndex = indexOfFirstSeparator;
                nextParameterStartIndex = indexOfFirstSeparator + MAIN_SPLIT_SYMBOL.length();
            } else {
                break;
            }
            String currentParameter = commandParameters.substring(currentParameterStartIndex, currentParameterStopIndex);
            parameters.add(currentParameter);
            commandParameters = commandParameters.substring(nextParameterStartIndex);
        }
        parameters.addAll(Arrays.asList(commandParameters.split(MAIN_SPLIT_SYMBOL)));
        parameters.removeAll(Arrays.asList(" ", "", null));
        return parameters;
    }

    private void print(String result) {
        System.out.println(result);
        System.out.println(REPORT_SEPARATOR);
    }
}

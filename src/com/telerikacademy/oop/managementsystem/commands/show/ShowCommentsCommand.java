package com.telerikacademy.oop.managementsystem.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Commentable;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowCommentsCommand implements Command {
    private static final String INVALID_TASK_ID = "Task ID %s should be a valid number";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String TASK_COMMENTS = "--Task with ID %d - COMMENTS--";
    public static final String TASK_NO_COMMENTS = "--Task with ID %d - NO COMMENTS--";

    private final ManagementSystemRepository managementSystemRepository;

    public ShowCommentsCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelpers.tryParseInt(arguments.get(0), String.format(INVALID_TASK_ID, arguments.get(0)));
        Commentable task = managementSystemRepository.findTaskById(id);
        String commentsTitle = "";

        if (task.getComments().isEmpty()){
            return String.format(TASK_NO_COMMENTS, id);
        }

        return String.format(TASK_COMMENTS, id) +
                ListingHelpers.stringToList(task.getComments());
    }

}

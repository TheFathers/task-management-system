package com.telerikacademy.oop.managementsystem.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.LogActivityAble;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ShowActivityCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String INPUT_ERR_MSG = "There is nothing with name %s.";

    private final ManagementSystemRepository managementSystemRepository;

    public ShowActivityCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        String[] names = arguments.get(0).split(",");

        List<LogActivityAble> objectToShowActivityHistory = new ArrayList<>();

        for (String name : names) {
            boolean isFound = false;
            if (managementSystemRepository.memberExists(name)) {
                Member member = managementSystemRepository.findMemberByName(name);
                objectToShowActivityHistory.add(member);
                isFound = true;
            }
            if (managementSystemRepository.teamExists(name)) {
                Team team = managementSystemRepository.findTeamByName(name);
                objectToShowActivityHistory.add(team);
                isFound = true;
            }
            if (managementSystemRepository.boardExists(name)) {
                Board board = managementSystemRepository.findBoardByName(name);
                objectToShowActivityHistory.add(board);
                isFound = true;
            }
            if (!isFound) {
                throw new ElementNotFoundException(String.format(INPUT_ERR_MSG, name));
            }
        }
        return showActivity(objectToShowActivityHistory);
    }

    private String showActivity(List<LogActivityAble> elements) {  //element could be team, member or board

        return elements.stream()
                .flatMap(element -> element.getActivity().stream())
                .map(Activity::viewInfo)
                .collect(Collectors.joining("\n"));

    }

}

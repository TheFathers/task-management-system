package com.telerikacademy.oop.managementsystem.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

public class AssignCommand implements Command {

    private static final String INVALID_TASK_ID = "Task ID %s should be a valid number";
    private static final String BUG_ASSIGN_SUCCESS = "Bug with ID %d assigned to %s.";
    private static final String STORY_ASSIGN_SUCCESS = "Story with ID %d assigned to %s.";
    private static final String TASK_ALREADY_ASSIGNED = "Task with %d is already assigned to member/s with name %s.";

    private final ManagementSystemRepository managementSystemRepository;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public AssignCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelpers.tryParseInt(arguments.get(0), String.format(INVALID_TASK_ID, arguments.get(1)));
        List<String> memberNames = Arrays.asList(arguments.get(1).split(","));
        return assignOneTask(id, memberNames);

    }

    private String assignOneTask(int id, List<String> memberNames) {

        List<Member> membersToAssign = tryParseMember(memberNames);
        throwsExceptionIfTaskAssignedToMember(id, membersToAssign, memberNames);
        Task task = managementSystemRepository.findTaskById(id);

        switch (task.getTaskType()) {
            case BUG:
                Bug bug = managementSystemRepository.findBugById(id);
                bug.setAssignees(membersToAssign);
                membersToAssign.forEach(member -> member.addTask(bug));
                return format(BUG_ASSIGN_SUCCESS, id, memberNames);
            case STORY:
                Story story = managementSystemRepository.findStoryById(id);
                story.setAssignees(membersToAssign);
                membersToAssign.forEach(member -> member.addTask(story));
                return format(STORY_ASSIGN_SUCCESS, id, memberNames);
            default:
                throw new InvalidUserInputException("Feedback cannot be assigned");
        }
    }

    private void throwsExceptionIfTaskAssignedToMember(int id, List<Member> members, List<String> memberNames) {
        Task task = managementSystemRepository.findTaskById(id);
        boolean isMemberAssigned = members.stream().anyMatch(member -> member.getTasks().contains(task));
        if (isMemberAssigned) {
            throw new InvalidUserInputException(String.format(TASK_ALREADY_ASSIGNED, id, memberNames));
        }
    }

    private List<Member> tryParseMember(List<String> names) {
        List<Member> members = new ArrayList<>();
        names.forEach(memberName -> members.add(managementSystemRepository.findMemberByName(memberName)));
        return members;
    }

}

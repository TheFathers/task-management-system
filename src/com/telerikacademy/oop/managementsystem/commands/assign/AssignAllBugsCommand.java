package com.telerikacademy.oop.managementsystem.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.Severity;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AssignAllBugsCommand implements Command {

    private static final String BUG_ALREADY_ASSIGNED = "Bugs with type %s is already assigned to member/s with name %s.";
    public static final String BUG_WITH_TYPE_ASSIGNED = "All bugs with type %s are assigned to %s.";
    public static final String NO_BUGS_OF_TYPE = "No bugs with this type: %s.";
    public static final String NO_BUGS_CREATED = "There are no bugs created of any kind.";

    private final ManagementSystemRepository managementSystemRepository;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public AssignAllBugsCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }


    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        List<String> memberNames = Arrays.asList(arguments.get(0).split(","));
        String bugFeature = arguments.get(1).toUpperCase();
        String featureType = arguments.get(2).toUpperCase();

        List<Bug> bugsToBeAssigned = getAllBugsOfUnknownType(bugFeature, featureType);
        List<Member> members = new ArrayList<>();
        memberNames.forEach(memberName -> members.add(managementSystemRepository.findMemberByName(memberName)));

        throwsExceptionIfBugAssignedToMember(bugsToBeAssigned, members, featureType);

        return assignMembersToBugs(bugsToBeAssigned, members, featureType);

    }

    private List<Bug> getAllBugsOfUnknownType(String bugFeature, String featureType) {

        List<Bug> allBugs = managementSystemRepository.getAllBugs();
        List<Bug> bugsToBeAssigned = new ArrayList<>();

        if (allBugs.isEmpty()) {
            throw new IllegalArgumentException(NO_BUGS_CREATED);
        }
        switch (bugFeature) {
            case "PRIORITY":
                Priority priority = ParsingHelpers.tryParseEnum(featureType, Priority.class);
                allBugs.forEach(bug -> {
                    if (bug.getPriority().equals(priority)) {
                        bugsToBeAssigned.add(bug);
                    }
                });
                return bugsToBeAssigned;
            case "SEVERITY":
                Severity severity = ParsingHelpers.tryParseEnum(featureType, Severity.class);
                allBugs.forEach(bug -> {
                    if (bug.getSeverity().equals(severity)) {
                        bugsToBeAssigned.add(bug);
                    }
                });
                return bugsToBeAssigned;
            case "BUGSTATUS":
                allBugs.forEach(bug -> {
                    if (bug.getStatus().equalsIgnoreCase(featureType)) {
                        bugsToBeAssigned.add(bug);
                    }
                });
                return bugsToBeAssigned;
            default:
                throw new InvalidUserInputException(String.format(NO_BUGS_OF_TYPE, featureType));
        }
    }

    private String assignMembersToBugs(List<Bug> bugsToBeAssigned, List<Member> members, String featureType) {
        bugsToBeAssigned
                .forEach(bug -> members.
                        forEach(member -> member.addTask(bug)));
        bugsToBeAssigned.forEach(bug -> bug.setAssignees(members));
        return String.format(BUG_WITH_TYPE_ASSIGNED, featureType, members);
    }

    private void throwsExceptionIfBugAssignedToMember(
            List<Bug> bugsToBeAssigned, List<Member> members, String featureType) {
        boolean isAssigned = bugsToBeAssigned
                .stream()
                .anyMatch(bug -> bug.getAssignees()
                        .stream()
                        .anyMatch(assignee -> members.stream()
                                .anyMatch(member -> member.getName().equals(assignee.getName()))));
        if (isAssigned) {
            throw new IllegalArgumentException(String.format(BUG_ALREADY_ASSIGNED, featureType, members));
        }
    }

}

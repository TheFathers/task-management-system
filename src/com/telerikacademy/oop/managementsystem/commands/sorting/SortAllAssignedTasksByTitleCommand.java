package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortAllAssignedTasksByTitleCommand implements Command {

    private final ManagementSystemRepository managementSystemRepository;

    public SortAllAssignedTasksByTitleCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {

        List<Member> allMembers = managementSystemRepository.getAllMembers();

        if(allMembers.isEmpty()){
            throw new IllegalArgumentException("There are no members.");
        }

        return "--Sorted by TITLE--\n" + allMembers
                .stream()
                .flatMap(member -> member.getTasks().stream())
                .sorted(Comparator.comparing(Task::getTitle))
                .map(Task::getImportantInfo)
                .collect(Collectors.joining("\n"));
    }
}

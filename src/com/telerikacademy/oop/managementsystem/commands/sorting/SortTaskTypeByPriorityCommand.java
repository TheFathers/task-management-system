package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortTaskTypeByPriorityCommand implements Command {
    public static final String INVALID_TASKTYPE = "Not a valid Type. " +
            "Please enter: Bug or Story. Feedbacks have no Priority.";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementSystemRepository managementSystemRepository;
    public SortTaskTypeByPriorityCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        TaskType taskType = ParsingHelpers.tryParseEnum(arguments.get(0),TaskType.class);

        if(managementSystemRepository.getAllBugs().isEmpty() &&
                managementSystemRepository.getAllStories().isEmpty()){
            throw new ElementNotFoundException("There are no tasks with priority.");
        }

        switch (taskType) {
            case BUG:
                List<Bug> allBugs = managementSystemRepository.getAllBugs();
                return "--Sorted Bugs by PRIORITY--\n" + allBugs
                        .stream()
                        .sorted(Comparator.comparing(Bug::getPriority))
                        .map(Task::getImportantInfo)
                        .collect(Collectors.joining("\n"));

            case STORY:
                List<Story> allStories = managementSystemRepository.getAllStories();
                return "--Sorted Stories by PRIORITY--\n" + allStories
                        .stream()
                        .sorted(Comparator.comparing(Story::getPriority))
                        .map(Task::getImportantInfo)
                        .collect(Collectors.joining("\n"));
            default:
                throw new InvalidUserInputException(INVALID_TASKTYPE);
        }
    }

}

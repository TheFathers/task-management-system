package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterAllTasksByStatusAndAssigneeCommand implements Command {
    public static final String INVALID_STATUS_INPUT = "Not a valid Status. Please enter corresponding status";
    public static final String NO_TASKS_WITH_STATUS = "--No tasks with status %s";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementSystemRepository managementSystemRepository;

    public FilterAllTasksByStatusAndAssigneeCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String assigneeName = arguments.get(0);
        String statusInput = arguments.get(1);

        Member assignee = managementSystemRepository.findMemberByName(assigneeName);

        if (assignee.getTasks().isEmpty()){
            throw new IllegalArgumentException(String.format("There are no assigned tasks to %s.", assigneeName));
        }
        StatusValidator.checkIfStatusIsValid(statusInput, INVALID_STATUS_INPUT);

        List<Task> filteredTasks = assignee.getTasks()
                .stream()
                .filter(task -> task.getStatus().equalsIgnoreCase(statusInput))
                .collect(Collectors.toList());

        if (filteredTasks.isEmpty()){
            throw new InvalidUserInputException(String.format(NO_TASKS_WITH_STATUS,statusInput));
        }

        return String.format("--All tasks filtered by %s & %s--%n", assigneeName, statusInput) +
                ListingHelpers.tasksToString(filteredTasks);
    }
}





